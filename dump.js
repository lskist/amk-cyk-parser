// dump.js - Rotinas para imprimir objetos e vetores.
// NOTA: Está uma carroça.

function pad(n) {
	var s = "";
	for (var i=0; i<n; i++)
		s+=" ";
	return s;
}

function isInt(x) {
	return !(x.match(/[^0-9]/));
}

function dump(x, indent) {
	if (x === null)
		return "null";
	else if (typeof(x) == 'function')
		return "function";
	else if (typeof(x) == 'string')
		return x.normalize();
	else if (typeof(x) == 'object')
		return x.toString(indent);
	else
		return String(x);
}

String.prototype.normalize = function() {
	var s = this;
	var from = ['\b', '\t', '\n', '\v', '\f', '\r'];
	var to = ['\\b', '\\t', '\\n', '\\v', '\\f', '\\r'];
	var fromrx = from.map(function(x) { return RegExp(x, "g") });

	var m, n;
	s = s.replace(/\\/g, "\\\\");
	for (var i in fromrx)
		s = s.replace(fromrx[i], to[i]);

	var m, n;
	while (m = s.match(/([\x00-\x1F])/)) {
		n = m[1].charCodeAt(0).toString(16);
		if (n.length < 2)
			n = 0 + n;
		s = s.replace(RegExp(m[1],"g"), "\\x"+n);
	}

	if (s.match(/"/)) {
		if (s.match(/'/))
			return '"' + s.replace(/"/g, '\\"') + '"';
		else
			return "'" + s + "'";
	}
	else
		return '"' + s + '"';
}

Array.prototype.toString = function(indent) {
	var listdump = "[";
	var dictdump = "[";
	var first = true;
	var islist = true;
	if (indent === undefined)
		indent = 0;

	for (var i in this) {
		if (!isInt(i))
			islist = false;
		if (first)
			first = false;
		else {
			listdump += ",";
			dictdump += ",\n" + pad(indent+1);
		}
		listdump += dump(this[i], indent+4);
		dictdump += dump(i) + ": " + dump(this[i], indent+3+dump(i).length);
	}
	listdump += "]";
	dictdump += "]";

	if (islist)
		return listdump;
	else
		return dictdump;
}

Object.prototype.toString = function(indent) {
	var s = "{\n"
	if (indent === undefined)
		indent = 0;
	indent += 4;
	var indt;

	for (var i in this) {
		if (this[i] instanceof Array)
			indt = indent + dump(i).length;
		else
			indt = indent;
		s += pad(indent) + i + ": " + dump(this[i], indt) + ",\n";
	}

	s += pad(indent-4) + "}";
	return s;
}

/* debugging.
a = []
a["foo"] = 1
a["bar"] = []
a["baz"] = 2
a["quux"] = [1,2,3]
a["hack"] = 3

a["bar"]["x"] = 23
a["bar"]["y"] = 42
a["bar"]["z"] = 69

q = ({x:4,y:a,z:6})
*/
