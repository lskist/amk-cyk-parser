// Grammar:
//  - vars: dicionário de booleanos (pesquisam-se as chaves; isto é, para
//    testar se uma variável existe, basta testar se g.vars["Var"] é true)
//  - terms: dicionário de booleanos (same as above)
//  - init: string
//  - rules: dicionário de vetores de vetores. cada posição contém um vetor
//    de todas as regras cujo LHS é a chave. Cada elemento desse vetor é um
//    vetor contendo a probabilidade da regra seguida dos símbolos do RHS da
//    regra.
//    Exemplo:
//     Gramática: [ VP ] > [ VT ] [ NP ] ;0.9
//                [ VP ] > [ Copula ] [ NP ] ;0.1
//     Estrutura: g.rules["VP"] = [ [0.9, "VT", "NP"],
//                                  [0.1, "Copula", "NP"] ];

load("grammar.js");

// Gerador de frases.
function generate(g, init, select) {
	if (g.terms[init])
		return [init];
	if (!g.vars[init])
		return [];
	
	var i = select(g.rules[init]);
	var j, res = [];

	for (j=1; g.rules[init][i][j]; j++)
		res = res.concat(generate(g, g.rules[init][i][j], select));
	return res;
}

// Seleciona aleatoriamente uma regra, respeitando as probabilidades.
function random_select(prods) {
	var rand = Math.random();
	var i;
	for (i=0; prods[i] && prods[i][0] < rand; i++)
		rand -= prods[i][0];
	if (!prods[i]) i--;
	if (!prods[i]) throw Error("Rule has a problem! " + prods);
	return i;
}

// Seleciona deterministicamente uma regra.
function deterministic_select(prods) {
	// Rotaciona a lista de produções e retorna a originalmente primeira.
	return prods.push(prods.shift()) - 1;
}

// Converte a lista de palavras em uma string bem-apresentável.
function formatsentence(ls) {
	var s = ls.join(" ");
	s = s[0].toUpperCase() + s.substr(1) + ".";
	return s;
}

